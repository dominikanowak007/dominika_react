import React, { Component } from 'react';
import {Jumbotron} from 'react-bootstrap';

class NavigationHeader extends Component {

    render() {
        return (
            <Jumbotron style={{backgroundColor: 'grey', borderRadius : '4px', margin :'.2%',padding:'2%',color:'lightyellow'}}>
            <h2 className="page-header">Welcome to the Bee Facts App</h2>
            <button className="btn btn-primary" onClick={this.props.clicked}>  *NEXT*  </button>
            <br></br> <br></br>
        </Jumbotron>
        )
    }
}

export default NavigationHeader;
