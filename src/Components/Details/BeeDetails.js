import React, { Component } from 'react';

class BeeDetails extends Component {
    render() {
        return (

                <table className="table table-hover" >
                    <thead>
                    <tr>
                        <th className ="table tblHeader"scope="row">{this.props.beeTitle}</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <th className="table tblPic"scope="row"><br/>{this.props.beePic}</th>
                        <th className="table tblFacts"scope="row"><br/>{this.props.beeFact}</th>
                    </tr>
                    </tbody>
                </table>

        );
    }
}

export default BeeDetails;
