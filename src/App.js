import React, { Component } from 'react';
import './App.css';
import NavigationHeader from "./Components/Header/NavigationHeader";
import BeeDetails from "./Components/Details/BeeDetails";

var beeFactsBook = {
    facts: [
        {
            id: 1,
            title: " *** Honey bees can fly at speeds of up to 15 miles per hour *** ",
            content: "Honey bees are built for short trips from flower to flower," +
            " not for long distance travel. Their tiny wings must flap about 12,000 " +
            "times per minute just to keep their pollen-laden bodies aloft for the flight home",
            pic: <img src={ require('./Components/Images/Bee1.png') } alt={"bee1"}  />

        },
        {
            id: 2,
            title:" *** A honey bee colony can contain up to 60,000 bees at its peak *** ",
            content:"Nurse bees care for the young, while the queen's attendant workers bathe and feed her. " +
            "Guard bees stand watch at the door. Construction workers build the beeswax foundation in" +
            " which the queen lays eggs and the workers store honey. Undertakers carry the dead from the " +
            "hive. Foragers must bring back enough pollen and nectar to feed the entire community",
            pic: <img src={ require('./Components/Images/Bee2.jpeg') } alt={"bee2"}/>
        },
        {
            id: 3,
            title:"*** A single honey bee worker produces about 1/12th of a teaspoon of honey in her lifetime ***",
            content: " From spring to fall, the worker bees must produce about 60 lbs." +
            " of honey to sustain the entire colony during the winter",
            pic: <img src={ require('./Components/Images/Bee3.jpeg') } alt={"bee3"}/>
        },
        {
            id: 4,
            title:" *** The Queen Honey Bee lays up to 1,500 eggs per day, and may lay up to 1 million in her lifetime ***",
            content: "Just 48 hours after mating, the queen begins her lifelong task of laying eggs. " +
            "So prolific an egg layer is she, she can produce her own body weight in eggs in a single day. " +
            "In fact, she has no time for any other chores, so attendant workers take care of all her grooming and feeding.",
            pic: <img src={ require('./Components/Images/Bee4.jpeg') } alt={"bee4"}/>
        }
    ]
}


class App extends Component {

    constructor() {
        super();
        this.state = {
            bees: beeFactsBook.facts,
            index: 0
        }
    }

    getNextBeeInfoIndex(){
        this.setState({
            index : ( this.state.index === beeFactsBook.facts.length - 1 ? 0 : this.state.index + 1)});
    }
    render() {
            return (
                <div className="container">

                    <div className="row">
                        <div className="col-12"> <NavigationHeader clicked={this.getNextBeeInfoIndex.bind(this)}/></div>
                    </div>

                    <div className="row">
                        <div className="col-12"><BeeDetails  beeTitle={this.state.bees[this.state.index].title} beeFact={this.state.bees[this.state.index].content} beePic = {this.state.bees[this.state.index].pic } /></div>
                    </div>
                </div>
            );}
}
export default App;
